// Board parameter
var HOLDER_SIZE = 100;
var HOLDER_MARGIN = 20;
var BOARD_COUNT = 4;
var BOARD_WIDTH = BOARD_HEIGHT = HOLDER_SIZE * BOARD_COUNT + HOLDER_MARGIN * (BOARD_COUNT + 1);
var OFFSET_X = HOLDER_MARGIN;
var OFFSET_Y = HOLDER_MARGIN;

exports.createView = function(opts) {
	var BG_WIDTH = opts.width;
	var BG_HEIGHT = opts.height;
	return { 
		children: [
			{
				name: "title",
				cls: 'ui.TextView',
				left: 0,
				y: 0,
				width: BG_WIDTH/3 - 10,
				height: BG_HEIGHT,
				text: "2048",
				horizontalAlign: 'center',
				verticalAlign: 'middle',
				backgroundColor: '#fadd6a',
				color: 'white',
				size: 60,
				fontWeight: 'bold',
				fontFamily: 'calibri'
			},
			{
				name: "thissession",
				cls: 'ui.View',
				left: 0,
				y: 0,
				width: BG_WIDTH/3 - 10,
				height: BG_HEIGHT,
				children: [
					{
						name: "score",
						cls: 'ui.View',
						x: 0,
						y: 0,
						width: BG_WIDTH/3 - 10,
						height: BG_HEIGHT/3 * 2 - 20,
						backgroundColor: '#baae9f',
						children: [
							{
								name: "scoreLabel",
								cls: 'ui.TextView',
								x: 0,
								y: 0,
								text: "Score",
								color: '#ede4da',
								size: 40,
								width: BG_WIDTH/3 - 10,
								height: BG_HEIGHT/9 * 2,
								horizontalAlign: 'center',
								verticalAlign: 'top'
							},
							{
								name: "scoreText",
								cls: 'ui.TextView',
								x: 0,
								y: BG_HEIGHT/9 * 2,
								text: "0",
								color: 'white',
								size: 60,
								width: BG_WIDTH/3 - 10,
								height: BG_HEIGHT/9 * 4 - 20,
								horizontalAlign: 'center',
								verticalAlign: 'top'
							},
						]
					},
					{
						name: "newGame",
						cls: 'ui.TextView',
						x: 0,
						y: BG_HEIGHT/3 * 2,
						horizontalAlign: 'center',
						color: 'white',
						verticalAlign: 'middle',
						width: (BG_WIDTH/3 - 10) / 2 - 5,
						height: BG_HEIGHT/3,
						size: 25,
						fontWeight: 'bold',
						text: "New",
						backgroundColor: '#ffba73'
					},
					{
						name: "Howto",
						cls: 'ui.TextView',
						x: (BG_WIDTH/3 - 10) / 2 + 5,
						y: BG_HEIGHT/3 * 2,
						horizontalAlign: 'center',
						color: 'white',
						verticalAlign: 'middle',
						width: (BG_WIDTH/3 - 10) / 2 - 5,
						height: BG_HEIGHT/3,
						size: 25,
						fontWeight: 'bold',
						text: "?",
						backgroundColor: '#ffba73'
					}					
				]
			},
			{
				name: "best",
				cls: 'ui.View',
				left: 0,
				y: 0,
				width: BG_WIDTH/3 - 10,
				height: BG_HEIGHT,
				children: [
					{
						name: "bestscore",
						cls: 'ui.View',
						x: 0,
						y: 0,
						width: BG_WIDTH/3 - 10,
						height: BG_HEIGHT/3 * 2 - 20,
						backgroundColor: '#baae9f',
						children: [
							{
								name: "scoreLabel",
								cls: 'ui.TextView',
								x: 0,
								y: 0,
								size: 40,
								text: "Best",
								color: '#ede4da',
								width: BG_WIDTH/3 - 10,
								height: BG_HEIGHT/9 * 2,
								horizontalAlign: 'center',
								verticalAlign: 'top'
							},
							{
								name: "scoreText",
								cls: 'ui.TextView',
								x: 0,
								y: BG_HEIGHT/9 * 2,
								text: "0",
								color: 'white',
								size: 60,
								width: BG_WIDTH/3 - 10,
								height: BG_HEIGHT/9 * 4 - 20,
								horizontalAlign: 'center',
								verticalAlign: 'top'
							},
						]
					},
					{
						name: "leaderboard",
						cls: 'ui.TextView',
						x: 0,
						y: BG_HEIGHT/3 * 2,
						color: 'white',
						size: 25,
						fontWeight: 'bold',
						backgroundColor: '#ffba73',
						horizontalAlign: 'center',
						verticalAlign: 'middle',
						width: (BG_WIDTH/3 - 10),
						height: BG_HEIGHT/3,
						text: "Leaderboard"
					}					
				]
			}			
		]
	}
}

