import device;
import animate;
import ui.View as View;
import ui.ViewPool as ViewPool;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

import src.topView as topView;
import src.lib.uiInflater as uiInflater;

import nativeframework.admob as admob;

// application
var app;
var isNative = GLOBAL.NATIVE && !device.simulatingMobileNative;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

// Board parameter
var HOLDER_SIZE = 100;
var HOLDER_MARGIN = 20;
var BOARD_COUNT = 4;
var BOARD_WIDTH = BOARD_HEIGHT = HOLDER_SIZE * BOARD_COUNT + HOLDER_MARGIN * (BOARD_COUNT + 1);
var OFFSET_X = HOLDER_MARGIN;
var OFFSET_Y = HOLDER_MARGIN;

// Built-in function
var floor = Math.floor;
var random = Math.random;
var rollFloat = function (mn, mx) { return mn + random() * (mx - mn); };
var rollInt = function (mn, mx) { return floor(mn + random() * (1 + mx - mn)); };

exports = Class(GC.Application, function () {

  this.initUI = function () {
    app = this;
    this.model = {
      current_score: 0,
      best_score: 0,
      gameover: false,
      gamewin: false,
      "color": {
        "2": "#ede4da",
        "4": "#ebe1c7",
        "8": "#edb275",
        "16": "#ef975e",
        "32": "#f07e5c",
        "64": "#f06135",
        "128": "#e9d06a",
        "256": "#e8ce57",
        "512": "#edb275",
        "1024": "#edb275",
        "2048": "#edb275"
      },
      "directionArray": {
        "up": {
          "x": 0,
          "y": -1,
          "xLimitMin": -1,
          "xLimitMax": BOARD_COUNT,
          "yLimitMin": 1,
          "yLimitMax": BOARD_COUNT - 1,
          "steps" : [
            {
              "operator": "divide",
              "value": 1,
            },
            {
              "operator": "divide",
              "value": 2,
            },
            {
              "operator": "divide",
              "value": 3,
            }            
          ]
        },
        "down": {
          "x": 0,
          "y": 1,
          "xLimitMin": -1,
          "xLimitMax": BOARD_COUNT,
          "yLimitMin": 0,
          "yLimitMax": BOARD_COUNT - 2,
          "steps" : [
            {
              "operator": "divide",
              "value": 2,
            },
            {
              "operator": "divide",
              "value": 1,
            },
            {
              "operator": "divide",
              "value": 0,
            }            
          ]          
        },
        "left": {
          "x": -1,
          "y": 0,
          "xLimitMin": 1,
          "xLimitMax": BOARD_COUNT - 1,
          "yLimitMin": -1,
          "yLimitMax": BOARD_COUNT,
          "steps" : [
            {
              "operator": "mode",
              "value": 1,
            },
            {
              "operator": "mode",
              "value": 2,
            },
            {
              "operator": "mode",
              "value": 3,
            }            
          ]          
        },
        "right": {
          "x": 1,
          "y": 0,
          "xLimitMin": 0,
          "xLimitMax": BOARD_COUNT - 2,
          "yLimitMin": -1,
          "yLimitMax": BOARD_COUNT,
          "steps" : [
            {
              "operator": "mode",
              "value": 2,
            },
            {
              "operator": "mode",
              "value": 1,
            },
            {
              "operator": "mode",
              "value": 0,
            }            
          ]                  
        }
      }
    };
    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    // blocks player input to avoid traversing game elements' view hierarchy
    this.bgLayer = new View({
      parent: this.view,
      y: this.view.style.height - BG_HEIGHT,
      backgroundColor: '#f9f8ee',
      width: BG_WIDTH,
      height: BG_HEIGHT
    });
    
    this.topView = new TopView({
      superview: this.bgLayer,
      width: BOARD_WIDTH,
      height: BG_HEIGHT - (BG_HEIGHT - this.view.style.height) - BOARD_HEIGHT - 250 - 50,  // -100 is the distance to the top of the device
      x: (BG_WIDTH - BOARD_WIDTH) / 2,
      y: BG_HEIGHT - this.view.style.height + 50,
      layout: 'linear',
      direction: 'horizontal',
      justifyContent: 'space'
    });

    this.howto = new HowToView({
      superview: this.view,
      x: 0,
      y: this.view.style.height - BG_HEIGHT,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      zIndex: 10000,
    });
    this.howto.style.visible = false;

    this.desc = new TextView({
      superview: this.bgLayer,
      width: BOARD_WIDTH,
      height: 100,
      size: 25,
      fontWeight: 'bold',
      fontFamily: 'calibri',
      x: (BG_WIDTH - BOARD_WIDTH) / 2,
      y: BG_HEIGHT - BOARD_HEIGHT - 250,
      horizontalAlign: 'center',
      verticalAlign: 'middle',
      text: "Join the numbers and get to the 2048 tile!"    
    });

    this.board = new View({
      superview: this.bgLayer,
      width: BOARD_WIDTH,
      height: BOARD_HEIGHT,
      x: (BG_WIDTH - BOARD_WIDTH) / 2,
      y: BG_HEIGHT - BOARD_HEIGHT - 150,
      backgroundColor: '#baae9f'
    });
    this.board.isReady = true;

    // a 0.5 opacity dark overley at game over
    this.gameOverLayer = new GameOverView({
      parent: this.bgLayer,
      width: BOARD_WIDTH,
      height: BOARD_HEIGHT,
      x: (BG_WIDTH - BOARD_WIDTH) / 2,
      y: BG_HEIGHT - BOARD_HEIGHT - 150,
      anchorX: BOARD_WIDTH / 2,
      anchorY: BOARD_HEIGHT / 2,
      zIndex: 9999
    });
    this.gameOverLayer.style.visible = false;

    this.itemPool = new ViewPool({
      ctor: BoardItem
    });

    this.board.on('InputStart', function(event, point) {
      if (!app.model.gameover) {
        this.startPoint = point;          
      }
    });

    this.board.on('InputMove', function(event, point) {
      this.currentPoint = point;
    });

    this.board.on('InputOut', function(over, overCount, atTarget) {
      if (!this.currentPoint || !this.startPoint || (this.model && this.model.gameover))
        return;

      var X = this.currentPoint.x - this.startPoint.x;
      var Y = this.currentPoint.y - this.startPoint.y;
      var direction = "left";
      if (X * X > Y * Y) {
        if (X > 0) direction = "right";
        else direction = "left";
      } else {
        if (Y > 0) direction = "down";
        else direction = "up";
      }
      app.board.emit('onSwipe', direction);
    });

    this.board.on('onSwipe', bind(this, function(direction) {
      if (this.board.isReady) {
        this.board.isReady = false;
        this.onSwipe(direction, 0);
      }
    }));

    this.board.on('onStepCollected', bind(this, function(direction, step) {
      this.onStepCollected(direction, step);
    }));
  };

  this.showBanner = function(_reload) {
    if (isNative)
      admob.showAdView({adSize: "banner", horizontalAlign: "center", verticalAlign: "bottom", reload: _reload});
  };

  this.RemoveHowto = function() {
    this.howto.style.visible = false;
    animate(this.bgLayer).now({opacity: 1}, 150);
  };

  this.launchUI = function () {
    for (var i = 0; i < BOARD_COUNT; i++) {
      for (var j = 0; j < BOARD_COUNT; j++) {
        var offsetX = OFFSET_X + i * ( HOLDER_MARGIN + HOLDER_SIZE );
        var offsetY = OFFSET_Y + j * ( HOLDER_MARGIN + HOLDER_SIZE );

        var item = new View({
          superview: this.board,
          x: offsetX,
          y: offsetY,
          width: HOLDER_SIZE,
          height: HOLDER_SIZE,
          backgroundColor: '#CCC1B3'
        });
      }
    };

    this.reset();
  };

  this.reset = function() {
    this.itemPool.releaseAllViews();
    this.boardItems = {};
    this.model.current_score = 0;
    this.model.gameover = false;
    this.model.gamewin = false;
    this.board.style.opacity = 1;
    this.gameOverLayer.style.visible = false;
    this.setupNewTable();
  };

  this.setupNewTable = function() {
    this.showBanner();
    // when start the game, 2 cells will have value in it

    // Random first Value
    var pos = rollInt(0, BOARD_COUNT * BOARD_COUNT - 1);
    var item = this.getItemAtIndex(pos, true);
    item.setValue(2);
    this.model.current_score += 2;

    //Random second Value
    var pos2 = rollInt(0, BOARD_COUNT * BOARD_COUNT - 1);
    while (pos2 == pos) {
      pos2 = rollInt(0, BOARD_COUNT * BOARD_COUNT - 1);
    }
    item = this.getItemAtIndex(pos2, true);
    var val = rollInt(0, 4);
    if (val < 1) {
      item.setValue(4);
      this.model.current_score += 4;
    }
    else {
      item.setValue(2);
      this.model.current_score += 2;
    }

    this.topView.thissession.score.scoreText.setText(this.model.current_score);
  };

  this.setupNewTableToDebug = function() {
    var pos = 9;
    var item = this.getItemAtIndex(pos, true);
    item.setValue(2);

    var pos2 = 13; 
    item = this.getItemAtIndex(pos2, true);
    item.setValue(2);   
  }

  this.getItemAtIndex = function(id, create) {
    if (!this.boardItems[id] && create) {
      var offsetX = OFFSET_X + (id % BOARD_COUNT) * ( HOLDER_MARGIN + HOLDER_SIZE );
      var offsetY = OFFSET_Y + (Math.floor(id / BOARD_COUNT)) * ( HOLDER_MARGIN + HOLDER_SIZE );      
      var item = this.itemPool.obtainView({
          superview: this.board,
          id: id,
          zIndex: 1000,
          x: offsetX,
          y: offsetY,
          width: HOLDER_SIZE,
          height: HOLDER_SIZE,
          backgroundColor: '#CCC1B3',
          canHandleEvents: false     
        });
      this.boardItems[id] = item;
    }

    return this.boardItems[id];
  };

  this.onSwipe = function(direction, step) {
    var operator = this.model.directionArray[direction].steps[step].operator;
    var val = this.model.directionArray[direction].steps[step].value;
    this.currentStepCollected = 0;

    for (var i = 0; i < BOARD_COUNT * BOARD_COUNT; i++) {
      var item = this.getItemAtIndex(i, false);
      if (operator == "mode") {
        if ((i % BOARD_COUNT) == val && this.isValid(i, direction)) {
          if (item)
            item.onSwipe(direction, step);
          else 
            this.onStepCollected(direction, step);
        } 
      } else {
        if ((Math.floor(i / BOARD_COUNT)) == val && this.isValid(i, direction)) {
          if (item)
            item.onSwipe(direction, step);
          else
            this.onStepCollected(direction, step);
        }
      }
    }
  }

  this.onStepCollected = function(direction, step) {
    this.currentStepCollected++;
    if (this.currentStepCollected == BOARD_COUNT && step < BOARD_COUNT - 2) {
      this.currentStepCollected = 0;
      this.onSwipe(direction, step + 1);
    } else if (this.currentStepCollected == BOARD_COUNT && step == BOARD_COUNT - 2) {
      var score = 0;
      var move = 0;
      for (var i = 0; i < BOARD_COUNT * BOARD_COUNT; i++)
        if (this.getItemAtIndex(i, false)) {
          this.getItemAtIndex(i, false).targetId = -1;
          score += this.getItemAtIndex(i, false).value;
          if (this.getItemAtIndex(i, false).isMoved) {
            move++; this.getItemAtIndex(i, false).isMoved = false;
          }
        }

      this.updateScore(score);

      var count = 0
      for (var i = 0; i < BOARD_COUNT * BOARD_COUNT; i++)
        if (this.getItemAtIndex(i, false)) count++;      
      var shouldSpawn = ((move > 0) || (count == BOARD_COUNT * BOARD_COUNT)) && !this.model.gamewin;
      if (shouldSpawn) {
        this.spawnPoint();
        this.board.isReady = true;
      }
      else if (this.model.gamewin) {
        this.GameOver(true);
      } else {
        this.board.isReady = true;
      }

    }
  }

  this.updateScore = function(newScore) {
    var score = newScore - this.model.current_score;
    this.model.current_score = newScore;
    this.topView.thissession.score.scoreText.setText(newScore);

    if (newScore > this.model.best_score) {
      this.model.best_score = newScore;
      this.topView.best.bestscore.scoreText.setText(newScore);
    }
  }

  this.spawnPoint = function() {
    var pos2 = rollInt(0, BOARD_COUNT * BOARD_COUNT - 1);
    var count = 0
    for (var i = 0; i < BOARD_COUNT * BOARD_COUNT; i++)
      if (this.getItemAtIndex(i, false)) count++;

    if (count < BOARD_COUNT * BOARD_COUNT) {
      while (app.boardItems[pos2]) {
        pos2 = rollInt(0, BOARD_COUNT * BOARD_COUNT - 1);
      }
      var item = this.getItemAtIndex(pos2, true);
      item.setValue(2);
      this.updateScore(this.model.current_score + 2);
    } else {
      // Check if Game is Over
      var gameover = true;
      
      for (var i = 0; i < BOARD_COUNT * BOARD_COUNT; i++) {
        // Check if it can move right
        if (this.isValid(i, "right")) {
          var directionFactor = app.model.directionArray["right"];
          var tempId = ((i % BOARD_COUNT) + directionFactor.x) + (Math.floor(i / BOARD_COUNT) + directionFactor.y) * BOARD_COUNT;
          if (this.getItemAtIndex(i, false).value == this.getItemAtIndex(tempId, false).value) {
            gameover = false;
            break;
          }
        }

        // Check if it can move down
        if (this.isValid(i, "down")) {
          var directionFactor = app.model.directionArray["down"];
          var tempId = ((i % BOARD_COUNT) + directionFactor.x) + (Math.floor(i / BOARD_COUNT) + directionFactor.y) * BOARD_COUNT;
          if (this.getItemAtIndex(i, false).value == this.getItemAtIndex(tempId, false).value) {
            gameover = false;
            break;
          }
        }        
      }

      if (gameover) {
        this.GameOver(false);
      }
    }
  }

  this.GameOver = function(win) {
    if (!this.model.gameover) {
      if (win) {
        this.gameOverLayer.setGameWin();
      } else {
        this.gameOverLayer.setGameLose();
      }
      this.model.gameover = true;
      var initialY = this.gameOverLayer.style.y;
      this.gameOverLayer.style.y = this.gameOverLayer.style.y + 100;
      this.gameOverLayer.style.visible = true;
      animate(this.gameOverLayer).now({y: initialY}, 150).then(function() {
        app.board.style.opacity = 0.5;
      });
    }
  }

  this.isValid = function(id, direction) {
    var x = id % BOARD_COUNT;
    var y = Math.floor(id / BOARD_COUNT);

    if (x < this.model.directionArray[direction].xLimitMin || x > this.model.directionArray[direction].xLimitMax)
      return false;
    if (y < this.model.directionArray[direction].yLimitMin || y > this.model.directionArray[direction].yLimitMax)
      return false;

    return true;
  }

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});


var BoardItem = Class(View, function(supr) {
  this.id = -1;
  this.targetId = -1;
  this.value = -1;
  this.isMoved = false;

  this.init = function(opts) {
    supr(this, 'init', [opts]);

    this.id = opts.id;
    this.text = new TextView({
      superview: this,
      width: HOLDER_SIZE,
      height: HOLDER_SIZE,
      size: 60,
      fontWeight: 'bold',
      horizontalAlign: "center",
      verticalAlign: "middle",
      text: "",
      color: "#766e65"
    });
  };

  this.updateOpts = function(opts) {
    supr(this, 'updateOpts', [opts]);

    this.id = opts.id;
    this.setValue(-1);
    this.isMoved = false;
  };

  this.setValue = function(val) {
    this.value = val;

    if (this.value > -1) {
      var str = "" + this.value;
      this.style.backgroundColor = app.model.color[str];
      this.text.setText(this.value);

      if (this.value == 2048) 
        app.model.gamewin = true;
    } else {
      this.text && this.text.setText("");
    }
  };

  this.onSwipe = function(direction, step) {
    var done = false;
    var tempId = this.id;
    var directionFactor = app.model.directionArray[direction];
    while(!done) {
      if (!app.isValid(tempId, direction)) 
        done = true;
      else {
        tempId = ((tempId % BOARD_COUNT) + directionFactor.x) + (Math.floor(tempId / BOARD_COUNT) + directionFactor.y) * BOARD_COUNT;

        if (app.getItemAtIndex(tempId, false)) {
          if (app.getItemAtIndex(tempId, false).value == this.value && app.getItemAtIndex(tempId, false).targetId == -1) {
            this.targetId = tempId;
          }
          done = true;
        } else {
          this.targetId = tempId;
        }
      }
    }

    if (this.targetId != -1) {
      var item = app.getItemAtIndex(this.targetId, true);
      item.style.zIndex = 999;
      animate(this).now({x: item.style.x, y: item.style.y}, 100, animate.easeOut).then(bind(this, function() {
        var reset = true;
        this.isMoved = true;
        if (item.value == this.value) {
          this.setValue(this.value * 2);
          reset = false;
        }
        item.removeFromSuperview();
        app.itemPool.releaseView(item);
        app.boardItems[this.id] = null;
        this.id = this.targetId;
        app.boardItems[this.targetId] = this;
        if (reset)
          this.targetId = -1; 
        app.board.emit('onStepCollected', direction, step);
      }));
    } else {
      app.board.emit('onStepCollected', direction, step);
    }
    
  };
});

var TopView = Class(View, function(supr) {
  this.init = function(opts) {
    supr(this, 'init', [opts]);

    this.buildView(opts);
  };

  this.buildView = function(opts) {
    var tv = topView.createView({width: this.style.width, height: this.style.height});
    uiInflater.addChildren(tv.children, this);

    this.thissession.newGame.on('InputSelect', function() {
      app.reset();
    });

    this.thissession.Howto.on('InputSelect', function() {
      animate(app.bgLayer).now({opacity: 0.5}, 150).then(function() {
        app.howto.style.visible = true;
      });    
    });
  }
});

var GameOverView = Class(View, function(supr) {
  this.init = function(opts) {
    supr(this, 'init', [opts]);

    this.buildView(opts);
  };

  this.buildView = function(opts) {
    this.gameOverTxt = new TextView({
      superview: this,
      x: 0,
      y: 0,
      width: this.style.width,
      height: this.style.height / 2,
      size: 60,
      fontWeight: 'bold',
      horizontalAlign: "center",
      verticalAlign: "middle",
      text: "Game Over!",
      color: "#766e65",
      zIndex: 10000
    });

    this.tryAgain = new TextView({
      superview: this,
      x: this.style.width / 4,
      y: this.style.height / 2,
      width: this.style.width / 2,
      height: this.style.height / 8,
      backgroundColor: "#766e65",
      size: 40,
      color: "white",
      text: "Try Again"
    });

    this.tryAgain.on('InputSelect', function() {
      app.reset();
    });
  };

  this.setGameWin = function() {
    this.gameOverTxt.setText("Congratulation!");
    this.tryAgain.setText("Play Again");
  };

  this.setGameLose = function() {
    this.gameOverTxt.setText("Game Over!");
    this.tryAgain.setText("Try Again");
  };
});

var HowToView = Class(View, function(supr) {
  this.init = function(opts) {
    supr(this, 'init', [opts]);

    this.buildView(opts);
  };

  this.buildView = function(opts) {
    this.layer = new View({
      superview: this,
      x: 0,
      y: 0,
      width: this.style.width,
      height: this.style.height,
      opacity: 0.5
    })

    this.rule1 = new ImageView({
      superview: this,
      x: (BG_WIDTH - 220 * 1.5) / 2,
      y: 200,
      width: 220 * 1.5,
      height: 229 * 1.5,
      image: "resources/images/rule1.png",
      zIndex: 10000
    });

    this.rule2 = new ImageView({
      superview: this,
      x: (BG_WIDTH - 216 * 1.5) / 2,
      y: 600,
      width: 216 * 1.5,
      height: 233 * 1.5,
      image: "resources/images/rule2.png",
      zIndex: 10000
    });

    this.on('InputSelect', bind(this, function() {
      if (this.style.visible) {
        app.RemoveHowto();
      }
    }));
  };
});