#2048 Game
Join the numbers and get to the 2048 tile!

#Project Location
Check out the project [here](https://bitbucket.org/tiendv/2048.git)

#Project Setup
##Requirement
* [Devkit](http://gameclosure.com/)
##Install Project
~~~
git clone https://bitbucket.org/tiendv/2048.git
~~~
At your project dir
~~~
devkit install
~~~
##Run Project
~~~
devkit serve
~~~
Open browser and visit: http://localhost:9200/